﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class LevelController : MonoBehaviour
{
    [SerializeField]
    private Transform[] enemyPositions;

    [SerializeField]
    private Ammo[] ammos;

    [SerializeField]
    private Transform characterPosition;

    [System.Serializable]
    public struct EnemyCount
    {
        public Enemy.enemyType type;
        public int count;
    }
    [System.Serializable]
    public struct Wave
    {
        public EnemyCount [] EnemyCounts;
    }
    public Wave[] waves;
    List<int> usedPositions = new List<int>();
    List<Enemy.enemyType> waveEnemyList = new List<Enemy.enemyType>();
    int currentWave = -1;
    int enemyIndex =0;
    bool newWave = false;
    [SerializeField]
    GameObject enemyParent;
    [SerializeField]
    Text nextWave_txt;
    void Start()
    {
        ReadWaves();
        NewWave();
        StartCoroutine(SpawnEnemies());
        StartCoroutine(SpawnAmmos());
    }
    void ReadWaves()
    {
        JsonReader.JsonData jsondata = JsonReader.instance.GetWaveRef();
        waves = new Wave[jsondata.data.waves.Count];
        for(int i =0;i<jsondata.data.waves.Count;i++)
        {
            waves[i].EnemyCounts = new EnemyCount[jsondata.data.waves[i].enemy.Count];
            for (int j =0; j < jsondata.data.waves[i].enemy.Count; j++)
            {
                waves[i].EnemyCounts[j].count = jsondata.data.waves[i].enemy[j].count;
                waves[i].EnemyCounts[j].type = (Enemy.enemyType)jsondata.data.waves[i].enemy[j].type - 1;
            }
        }
    }
    IEnumerator SpawnEnemies()
    {
        while(true)
        {
            if (newWave)
            {
                if (enemyParent.transform.childCount == 0)
                {
                    nextWave_txt.text = "Wave  " + (currentWave + 1).ToString();
                    nextWave_txt.gameObject.SetActive(true);
                    yield return new WaitForSeconds(1f);
                    nextWave_txt.gameObject.SetActive(false);
                    yield return new WaitForSeconds(2f);
                    newWave = false;
                }
            }
            else
            {
                OnSpawnEnemies(GetRandomEnemyCount());
                yield return new WaitForSeconds(Random.Range(4,8));
            }
            yield return null;
        }
    }
    IEnumerator SpawnAmmos()
    {
        int count = 0; 
        while (true)
        {
                int randomAmmo = Random.Range(0, ammos.Length);
                while (ammos[randomAmmo].gameObject.activeInHierarchy && count < 5)
                {
                    count++;
                    randomAmmo = Random.Range(0, ammos.Length);
                }
                if (count < 5)
                {
                    count = 0;
                    ammos[randomAmmo].Init();
                    yield return new WaitForSeconds(Random.Range(8, 14));
                }
 
            yield return null;
        }
    }
    void OnSpawnEnemies(int enemyCount)
    {
        int randomPosition;
        for(int i=0;i<enemyCount;i++)
        {
            randomPosition = Random.Range(0, enemyPositions.Length);
            while(usedPositions.Contains(randomPosition))
            {
                randomPosition = Random.Range(0, enemyPositions.Length);
            }
            Enemy enemy = EnemyPool.instance.GetEnemy();
            enemy.transform.parent = enemyParent.transform;
            enemy.Init(characterPosition.position - enemyPositions[randomPosition].position, waveEnemyList[enemyIndex++]);
            enemy.transform.position = enemyPositions[randomPosition].position;
            usedPositions.Add(randomPosition);
        }
        usedPositions.Clear();
        if (enemyIndex == waveEnemyList.Count)
            NewWave();
    }
    void NewWave()
    {
        newWave = true;
        enemyIndex = 0;
        currentWave += 1;
        if (currentWave >= waves.Length)
            currentWave = waves.Length - 1;
        RandomEnemyList();
    }
    int GetRandomEnemyCount()
    {
        int random =  Random.value > 0.5f ? Random.Range(1, enemyPositions.Length / 2) :
            Random.Range(enemyPositions.Length / 2 , enemyPositions.Length);
        if (random + enemyIndex >= waveEnemyList.Count)
            random = waveEnemyList.Count - enemyIndex ;
        return random;
    }
    void RandomEnemyList()
    {
        waveEnemyList.Clear();
        for (int i = 0; i < waves[currentWave].EnemyCounts.Length; i++)
        {
            for (int j = 0; j < waves[currentWave].EnemyCounts[i].count; j++)
            {
                waveEnemyList.Add(waves[currentWave].EnemyCounts[i].type);
            }
        }
        Shuffle(waveEnemyList);
    }
    void Shuffle(List<Enemy.enemyType> enemyList)
    {
        for (int i = enemyList.Count - 1; i > 0; i--)
        {
            int j = Random.Range(0, i + 1);
            Enemy.enemyType temp = enemyList[i];
            enemyList[i] = enemyList[j];
            enemyList[j] = temp;
        }
    }
 
   
}
