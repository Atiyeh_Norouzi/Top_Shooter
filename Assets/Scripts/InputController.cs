﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    public enum InputType { Shoot , RotateLeft , RotateRight , SwitchWeapon , Restart}
    public delegate void Deleg_Input(InputType type);
    public event Deleg_Input onClicked;

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
            if (onClicked != null)
                onClicked(InputType.Shoot);
        if (Input.GetMouseButtonDown(1))
            if (onClicked != null)
                onClicked(InputType.Shoot);
        if (Input.GetKey(KeyCode.A))
            if (onClicked != null)
                onClicked(InputType.RotateLeft);
        if (Input.GetKey(KeyCode.D))
            if (onClicked != null)
                onClicked(InputType.RotateRight);
        if (Input.GetKeyDown(KeyCode.R))
            if (onClicked != null)
                onClicked(InputType.SwitchWeapon);
        if (Input.GetKeyDown(KeyCode.Return))
            if (onClicked != null)
                onClicked(InputType.Restart);


    }
    void OnLevelWasLoaded()
    {
        onClicked = null;
    }
}
