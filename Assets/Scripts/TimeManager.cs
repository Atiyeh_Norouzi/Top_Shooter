﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager : MonoBehaviour
{
    public static float timeScale = 1;
    public static float deltaTime
    {
        get
        {
            return Time.deltaTime * timeScale;
        }
    }

}
