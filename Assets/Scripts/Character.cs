﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Character : MonoBehaviour
{
    private string[] _animationNames = { "Wait", "Attack", "Dead", "Damage" };

    [SerializeField]
    private InputController inputController;

    [SerializeField]
    private Transform targetPoint;

    [SerializeField]
    private Transform bulletStartPosition;

    [SerializeField]
    private WeaponManager weaponManager;

    [SerializeField]
    private Image healthbar_slider;

    bool MouseControl = false;

    Animation animation;

    void Start()
    {
        animation = GetComponent<Animation>();
        inputController.onClicked += InputListener;
    }
    void InputListener(InputController.InputType type)
    {
        if (type == InputController.InputType.Shoot)
            Shoot();
        if (!MouseControl)
        { 
            if (type == InputController.InputType.RotateLeft)
                Rotate(Vector3.forward);
            else if (type == InputController.InputType.RotateRight)
                Rotate(-Vector3.forward);
        }
    }
    public void OnKeyBoardControl()
    {
        targetPoint.gameObject.SetActive(false);
        GameState.instance.StartGame();
    }
    public void OnMouseControl()
    {
        StartCoroutine(RotateToTarget());
        MouseControl = true;
        GameState.instance.StartGame();
    }
    void Shoot()
    {
        playCharacterAnimation(1, QueueMode.PlayNow);
        weaponManager.InitBullet(targetPoint.position);
    }
    IEnumerator RotateToTarget()
    {
        while (true)
        {
            float angle = Mathf.Atan2(targetPoint.position.y - transform.position.y, targetPoint.position.x - transform.position.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.AngleAxis(angle -90, Vector3.forward);
            yield return null;
        }
    }
    void playCharacterAnimation(int index, QueueMode mode)
    {
        animation.PlayQueued(_animationNames[index], mode);
    }
    void Rotate(Vector3 dir)
    {
        transform.Rotate(dir * Time.deltaTime*200f, Space.World);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Enemy")
        {
            healthbar_slider.fillAmount -= collision.GetComponent<Enemy>().damage[collision.GetComponent<Enemy>().type];
            EnemyPool.instance.PushEnemy(collision.GetComponent<Enemy>());
            if (healthbar_slider.fillAmount <= 0)
            {
                playCharacterAnimation(2, QueueMode.CompleteOthers);
                GameState.instance.Pause();
            }

        }
    }

}
