﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletPool : MonoBehaviour {
    public enum bulletType { Bullet1 = 0, Bullet2, Bullet3 };

 [System.Serializable]
 public struct BulletObjStruct {
 public int amount;
 public GameObject prefab;
 public Stack<Bullet> pooledObjs;
 public GameObject bulletParent;
    }
    public BulletObjStruct[] bulletsWeWantToPool;

    private static BulletPool _instance;
    public static BulletPool instance {
        get {
            if (_instance == null)
                _instance = FindObjectOfType<BulletPool> ();

            return _instance;
        }
    }
    void Awake () {
        _instance = this;
        CreatePools ();
    }
    public void CreatePools () {
        createBullets ();
    }
    public void createBullets () {
        for (int i = 0; i < bulletsWeWantToPool.Length; i++) {
            bulletsWeWantToPool[i].pooledObjs = new Stack<Bullet> ();
            bulletsWeWantToPool[i].bulletParent = new GameObject (bulletsWeWantToPool[i].prefab.name + "_parent");
            bulletsWeWantToPool[i].bulletParent.transform.SetParent (instance.gameObject.transform);

            for (int j = 0; j < bulletsWeWantToPool[i].amount; j++) {

                GameObject GO = Instantiate (bulletsWeWantToPool[i].prefab);
                Bullet c = GO.GetComponent<Bullet> ();
                GO.SetActive (false);
                GO.transform.SetParent (bulletsWeWantToPool[i].bulletParent.transform);
                bulletsWeWantToPool[i].pooledObjs.Push (c);
            }
        }

    }

    public void pushBullet (bulletType type, Bullet bullet) {
        bulletsWeWantToPool[(int) type].pooledObjs.Push (bullet);
        bullet.gameObject.transform.SetParent (bulletsWeWantToPool[(int) type].bulletParent.transform);
        bullet.gameObject.transform.localPosition = Vector3.zero;
        bullet.gameObject.SetActive (false);
    }

    public Bullet GetBullet (bulletType type) {
        Bullet p = bulletsWeWantToPool[(int) type].pooledObjs.Pop ();
        p.gameObject.SetActive (true);
        p.gameObject.transform.SetParent (null);
        return p;
    }

}